﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Numerics;


public class UnicodeBasedId : MonoBehaviour
{
    [Header("Debug")]
    public string m_baseUsed = "☗☖♚♛♜♝♞♟♔♕♖♗♘♙0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ♭♯♮♩♪♫♬☢☠✈✇☎〒✓❄";
    public int m_basedLevel;

   public void Clean()
    {
        m_baseUsed = "";
    }

    public string m_maxulong;
    public string m_maxInt;


    public bool TryToConvertDecimalTextToUnicodeWithLong(string text, out string unicodResult)
    {
        ulong value = 0;
        if (!ulong.TryParse(text, out value))
        {
            unicodResult = "";
            return false;
        }
        unicodResult = ConvertLongToUnicode(value);
        return true;
    }

   public void AddCharacters(string characters)
    {
        m_baseUsed += characters;
        m_baseUsed = FilterChar(m_baseUsed);
        RefreshInformativeGauge();
    }

    public bool TryToConvertDecimalTextToUnicodeWithBigInteger(string text, out string unicodResult)
    {
        TryToConvertDecimalTextToUnicode(text, out unicodResult);
        return true;
    }


    public void TryToConvertUnicodeTextToDecimal(string unicodeValue, out string decimalValue)
    {
        BigInteger result;
        TryToConvertUnicodeToBigNumber(unicodeValue, out result);
        decimalValue = result.ToString();
    }
    public void TryToConvertUnicodeToBigNumber(string unicodeValue, out BigInteger decimalValue)
    {
        TryToConvertUnicode(unicodeValue, out decimalValue);
    
    }

    public void TryToConvertDecimalTextToUnicode(string decimalValue, out string unicodeValue)
    {
        TryToConvertBigNumberToUnicode(BigInteger.Parse(decimalValue), out unicodeValue);
    }
    public void TryToConvertBigNumberToUnicode(BigInteger unicodeValue, out string decimalValue)
    {
       decimalValue  =EncodeInBaseNWithBigInteger(unicodeValue, ref m_baseUsed);
        
    }


    public string ConvertLongToUnicode(ulong value)
    {
        return EncodeInBaseNWithLong(value, ref m_baseUsed);
    }

    public string FilterNotUsedUnicode(string unicodeNumber) {
        List<char> numbers = unicodeNumber.ToCharArray().ToList<char>();
        for (int i = numbers.Count - 1; i >= 0; i--)
        {
            if (m_baseUsed.IndexOf(numbers[i]) < 0)
                numbers.RemoveAt(i);
        }
        return string.Join("", numbers);
    }

    public bool TryToConvertUnicode(string unicodeNumber, out ulong value, bool removeUnkownListedUnicode = false)
    {
        if (removeUnkownListedUnicode)
            unicodeNumber = FilterNotUsedUnicode(unicodeNumber);

        int level = m_baseUsed.Length;
        int numberSize = unicodeNumber.Length;

        value = 0;
        ulong index = 0;
        for (int i = 0; i < numberSize; i++)
        {
            char c = unicodeNumber[numberSize - 1 - i];
            index = (ulong)m_baseUsed.IndexOf(c);
            //Debug.Log("C: " + unicodeNumber + " Index:" + index);
            if (index < 0)
                index = 0;
            value += index * ((ulong)Mathf.Pow(level, i));
        }

        return true;
    }
    BigInteger level;
    BigInteger numberSize;
    BigInteger index;
    BigInteger i = 0;
    BigInteger one = 1;
    public bool TryToConvertUnicode(string unicodeNumber, out BigInteger value, bool removeUnkownListedUnicode = false)
    {
        if (removeUnkownListedUnicode)
            unicodeNumber = FilterNotUsedUnicode(unicodeNumber);

         level = m_baseUsed.Length;
         numberSize   = unicodeNumber.Length;
         index        = 0;

        value = 0;
        for (i = 0; i < numberSize; i++)
        {
            char c = unicodeNumber[(int)(numberSize - i - one)];
            index = m_baseUsed.IndexOf(c);
            //Debug.Log(">Index: " + index + "->" + c);
            //Debug.Log("C: " + unicodeNumber + " Index:" + index);
            if (index < 0)
                index = 0;
            value += index * BigPow(level, i);//((ulong)Mathf.Pow(level, i));
        }

        return true;
    }

    private BigInteger BigPow(long value, long index)
    {
        return BigPow(new BigInteger(value), new BigInteger(index));
    }
    private BigInteger BigPow(BigInteger value, BigInteger index)
    {
        BigInteger result = 1;
        for (int i = 0; i < index; i++)
        {

            result *= value;
        }
        return result;
            
    }
    

    public string GetUnicode(int index)
    {
        if (m_baseUsed.Length == 0)
            return "";
        index = Mathf.Clamp(index, 0, m_baseUsed.Length-1);
        return ""+m_baseUsed[index];
    }

    public string GetMaxUnicode()
    {
        if (m_baseUsed.Length == 0)
            return "";
        return ""+m_baseUsed[m_baseUsed.Length - 1];
    }

    public string GetZeroUnicode()
    {
        if (m_baseUsed.Length == 0)
            return "";
        return "" + m_baseUsed[0];
    }

    public int GetRandomIndex()
    {
        if (m_baseUsed.Length == 0)
            return 0;

        return UnityEngine.Random.Range(0, m_baseUsed.Length);
    }

    public char GetRandomUnicode()
    {
        return m_baseUsed[GetRandomIndex()];
    }

    public string Convert(int value)
    {
       return  ConvertLongToUnicode((ulong) value);
    }

    public int GetBaseCount()
    {
        return m_baseUsed.Length;
    }

    public string GetBaseUsed()
    {
        return m_baseUsed;
    }

    public static string EncodeInBaseNWithLong(ulong value, ref string baseUsed)
    {
        //Source of the algo https://runestone.academy/runestone/books/published/pythonds/Recursion/pythondsConvertinganIntegertoaStringinAnyBase.html
        ulong baseLenght = (ulong)baseUsed.Length;
        if (value < baseLenght)
            return "" + baseUsed[(int)value];
        else return EncodeInBaseNWithLong(value / baseLenght, ref baseUsed) + ("" + baseUsed[(int)(value % baseLenght)]);
    }
    public static string EncodeInBaseNWithBigInteger(BigInteger value, ref string baseUsed)
    {
        //Source of the algo https://runestone.academy/runestone/books/published/pythonds/Recursion/pythondsConvertinganIntegertoaStringinAnyBase.html
        BigInteger baseLenght = new BigInteger(baseUsed.Length);
        if (value < baseLenght)
            return "" + baseUsed[(int)value];
        else return EncodeInBaseNWithBigInteger(BigInteger.Divide(value, baseLenght), ref baseUsed) + ("" + baseUsed[(int)(value % baseLenght)]);
    }


    public TextAsset m_toAdd;
    public void OnValidate() {
        if (m_toAdd != null)
        {
            m_baseUsed += FilterChar(m_toAdd.text);
            RefreshInformativeGauge();
            m_toAdd = null;
        }
        RefreshInformativeGauge();
    }
    
    private void RefreshInformativeGauge()
    {
        m_basedLevel = m_baseUsed.Length;
        m_maxulong = ConvertLongToUnicode(ulong.MaxValue-1);
        m_maxInt = Convert(int.MaxValue-1);
    }
    

   

    private string FilterChar(string text)
    {


       text = text.Replace(" ", "").Replace("\t", "").Replace("\r", "").Replace("\n", "");
       return new String(text.Distinct().ToArray());
        //return string.Join("", text.ToCharArray().Distinct().ToList());
    }
}
