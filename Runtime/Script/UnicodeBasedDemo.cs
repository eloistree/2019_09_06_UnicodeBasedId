﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class UnicodeBasedDemo : MonoBehaviour
{
    public UnicodeBasedId m_unicodeBased;
    [Header("Decimal to Unicode")]
    public InputField m_inDecimal;
    public InputField m_outUnicodResult;

    [Header("Unicode to Decimal")]
    public InputField m_inUnicode;
    public InputField m_outDecimalResult;
    public Slider m_unicodeSlider;
    public bool m_setMaxAtStart=true;

    private void Start()
    {
        if(m_unicodeSlider)
        m_unicodeSlider.maxValue = m_unicodeBased.GetBaseCount() - 1;
        if (m_setMaxAtStart) {
            m_inDecimal.text = "" + (m_unicodeBased.GetBaseCount() - 1);
            ConvertDecimalToUnicode();
            m_inUnicode.text = m_outUnicodResult.text;
        }


    }

    public void ConvertDecimalToUnicode()
    {
        string txt="";
        m_unicodeBased.TryToConvertDecimalTextToUnicode(m_inDecimal.text, out txt);
        m_outUnicodResult.text = txt;

    }
    public void ConvertUnicodeToDecimal()
    {
        m_inUnicode.text = m_unicodeBased.FilterNotUsedUnicode(m_inUnicode.text);
        BigInteger value = 0;
        m_unicodeBased.TryToConvertUnicodeToBigNumber(m_inUnicode.text, out value);
         m_outDecimalResult.text = value.ToString();
    }
    public void SetDecimal(float index)
    {
        SetDecimal((int)index);
    }
    public void SetDecimal(int index)
    {
        m_inDecimal.text = "" + index;
        m_inUnicode.text = m_unicodeBased.GetUnicode(index);
    }
    public void SetRandomUnicode()
    {
        m_inUnicode.text += "" + m_unicodeBased.GetRandomUnicode();
    }
    public void SetRandomDecimal()
    {
        m_inDecimal.text += "" + m_unicodeBased.GetRandomIndex();
    }
    public void FlushUnicode()
    {
        m_inUnicode.text = "";
    }
    public void FlushDecimal()
    {
        m_inDecimal.text = "0";
    }
    public void SetMaxUnicode()
    {
        m_inUnicode.text =  m_unicodeBased.GetMaxUnicode();
        ConvertUnicodeToDecimal();
    }
    public void SetZeroUnicode()
    {
        m_inUnicode.text = m_unicodeBased.GetZeroUnicode();
        ConvertUnicodeToDecimal();
    }

    public void SetIntMax() { m_inDecimal.text = "" + (int.MaxValue - 1); ConvertDecimalToUnicode(); }
    public void SetLongMax() { m_inDecimal.text = "" + (long.MaxValue - 1); ConvertDecimalToUnicode(); }
}
