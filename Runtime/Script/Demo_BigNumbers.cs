﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class Demo_BigNumbers : MonoBehaviour
{

    public UnicodeBasedId m_unicode;

    public InputField m_decimalInput;
    public InputField m_unicodeInput;

    public Text m_decimalCount;
    public Text m_unicodeCount;
    public Text m_multipleDebug;


    public void AddRandomUnicode()
    {
        m_unicodeInput.text += m_unicode.GetRandomUnicode();
    }
    public void AddMaxUnicode()
    {
        m_unicodeInput.text += m_unicode.GetMaxUnicode();
    }


    void Start()
    {
        m_decimalInput.onEndEdit.AddListener(ConvertToChinais);
        m_unicodeInput.onEndEdit.AddListener(ConvertToDecimal);
        m_decimalInput.onValueChanged.AddListener(RefreshCount);
        m_unicodeInput.onValueChanged.AddListener(RefreshCount);

    }

    private void RefreshCount(string arg0)
    {

        m_decimalCount.text = "" + m_decimalInput.text.Length;
        m_unicodeCount.text = "" + m_unicodeInput.text.Length;
        m_multipleDebug.text = "" + ((float)m_decimalInput.text.Length / (float)m_unicodeInput.text.Length);
    }

    public void ConvertToDecimal(string arg0)
    {
        string text = ""; 
        m_unicode.TryToConvertUnicodeTextToDecimal(arg0.Replace(" ",""), out text);
        m_decimalInput.text = text;
    }

    public void ConvertToChinais(string arg0)
    {

        string text = "";
        m_unicode.TryToConvertDecimalTextToUnicodeWithBigInteger(arg0.Replace(" ", ""), out text);
        m_unicodeInput.text = text;
    }
}
