﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UnicodeBasedId))]
public class UnicodeBasedIdEditor : Editor
{
    public static bool b_sub;
    public override void OnInspectorGUI()
    {
        b_sub = EditorGUILayout.Foldout(b_sub, "Edit Unicode");
        if (b_sub)
        {

        UnicodeBasedId myScript = (UnicodeBasedId)target;

            DrawDefaultInspector();
            EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Chess"))
        {
            myScript.AddCharacters("♚♛♜♝♞♟♔♕♖♗♘♙");
            }
        if (GUILayout.Button("0-9"))
        {
            myScript.AddCharacters("0123456789");
            }
        if (GUILayout.Button("a-Z"))
        {
            myScript.AddCharacters( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        
            if (GUILayout.Button("Clear"))
            {
                myScript.Clean();
            }
          
            if (GUILayout.Button("Save"))
            {
                string path = Application.dataPath + "/UnicodeBasedId.txt";
                File.WriteAllText(path, myScript.m_baseUsed);
                Debug.Log("Path:" + path);
            }
        EditorGUILayout.EndHorizontal();
        }
    }
}
